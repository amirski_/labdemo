drop table export_table;
create table export_table as
select *
from kazpost_addresses
where elastic_index is not null
limit 10000;

select * from export_table limit 100
"{"name": "2 УЛИЦА Хожамьярова", "source": "2gis", "address_parts": {"zip": "A36F9M5", "number": "2", "street": "УЛИЦА Хожамьярова"}, "source_id": "A36F9M5", "center_point": {"lon": "76.929372", "lat": "43.329485"}, "geohash": "txwwqxfk833m"}"
truncate labdemo_kazpost
INSERT INTO labdemo_kazpost(post_index, region, area, locality, district, street, house, geojson)
  SELECT post_index, left(region,50), left(area,50), left(locality,50), left(district,50), left(street,50), left(house, 15), gis_json::json#>'{geometry,centroid}'  FROM export_table
  where gis_json is not null and is_json(gis_json)
  limit 100;

INSERT INTO labdemo_kazpost(post_index, region, area, locality, district, street, house, geojson, lat, lon)
  SELECT post_index, left(region,50), left(area,50), left(locality,50), left(district,50), left(street,50), left(house, 15), elastic_index
	, replace((elastic_index::json#>'{center_point,lat}')::text,'"','')::float
	, replace((elastic_index::json#>'{center_point,lon}')::text,'"','')::float
  FROM export_table
  where elastic_index is not null
  limit 1000;


SELECT post_index, left(region,50), left(area,50), left(locality,50), left(district,50), left(street,50), left(house, 15), elastic_index
	, replace((elastic_index::json#>'{center_point,lat}')::text, '"','')::float
  FROM export_table
  where elastic_index is not null
  limit 1000;


SELECT post_index, region, area, locality, district, street, house,
       geojson, lat, lon, point
  FROM labdemo_kazpost
  limit 100;
  "{"name": "59 УЛИЦА ГРИБОЕДОВА", "source": "2gis", "address_parts": {"zip": "M01M0F5", "number": "59", "street": "УЛИЦА ГРИБОЕДОВА"}, "source_id": "M01M0F5", "center_point": {"lon": "73.046653", "lat": "49.788159"}, "geohash": "v8ggnpwvn3s0"}"

  select * from labdemo_kazpost