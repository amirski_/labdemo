from rest_framework.views import APIView
from labdemo.models import kazpost
from labdemo.serializers import kazpostSerializer
from rest_framework.response import Response
from django.views.generic import TemplateView
from django.views.decorators.clickjacking import xframe_options_exempt
from django.utils.decorators import method_decorator

class SearchView(APIView):
    @method_decorator(xframe_options_exempt)
    def get(self, request):
        term = request.GET.get('text')
        addrs = kazpost.es_search(term)
        kazpost_serializer = kazpostSerializer(addrs, many=True)
        response = {}
        response['addrs'] = kazpost_serializer.data


        return Response(response)


class IndexView(TemplateView):
    template_name = 'index.html'